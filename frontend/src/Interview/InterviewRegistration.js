import React, { useState } from 'react';
import axios from 'axios';
// import { DateTimePicker, KeyboardDatePicker } from "@material-ui/pickers";


export default class InterviewRegistration extends React.Component {

  state = {
    candidateId: '',
    modeOfInterview: '',
    dateOfInterview: Date,
    location: '',
    reccommendation: '',
    interviewPanel: '',
    referencesBy: '',
    id:'',


  };
  handleClear = event => {
    this.setState(
      {
        candidateId: '',
        modeOfInterview: '',
        dateOfInterview: '',
        location: '',
        reccommendation: '',
        interviewPanel: '',
        referencesBy: '',

      }
    );
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
    // this.setState({ modeOfInterview: event.target.value });
    // this.setState({ dateOfInterview: event.target.value });
    // this.setState({ location: event.target.value });
    // this.setState({ reccommendation: event.target.value });
    // this.setState({ interviewPanel: event.target.value });
    // this.setState({ referencesBy: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    // console.log(this.state.candidateId,this.state.modeOfInterview);
    var today = new Date();
    var interdate = new Date(this.state.dateOfInterview)
    console.log(interdate > today);
    let i = this.state;
    axios.post('http://localhost:9080/interview', i)
      .then(res => {
        if ((res.status == 200) && (interdate > today)) {
          alert("Form submitted successfully");
          window.location.replace("http://localhost:3000/ViewInterview");
        }
        else {
          alert("invalidate date");
        }
      }).catch(res => {
        alert("invalidate candidate Id");
      })


  }




  render() {
    const today = new Date();


    return (

      <div>
        <form onSubmit={this.handleSubmit} class="col-lg-6 offset-lg-3">

          <label><span class="required">*</span> Candidate Id:  <input type="text" class="form-control" name="candidateId" placeholder="----your Aadhar Id ----" onChange={this.handleChange} pattern="[0-9]{12}" title="enter the numbers only" maxlength="12" required /></label><br />
          <label ><span class="required">*</span> Mode Of Interview:  <input type="text" class="form-control" placeholder="Mode of Interview" name="modeOfInterview" onChange={this.handleChange} required /></label><br />
          <label><span class="required">*</span> Date OfInterview:  <input type="date" class="form-control" placeholder="Enter the Date" name="dateOfInterview" onChange={this.handleChange} required /></label><br />
          <label ><span class="required">*</span> Location:  <input type="text" class="form-control" name="location" pattern="[A-Za-z//s]*" title="enter the alphbets only" placeholder="location" onChange={this.handleChange} required /></label><br />
          <label>Reccommendation:<input type="text" class="form-control" name="reccommendation" pattern="[A-Za-z//s]*" title="enter the alphbets only" placeholder="reccommendation" onChange={this.handleChange} /></label><br />
          <label><span class="required">*</span> Interview Panel:  <input type="text" class="form-control" name="interviewPanel" placeholder="interview" onChange={this.handleChange} required /></label><br />
          <label>References By:  <input type="text" class="form-control" name="referencesBy" pattern="[A-Za-z//s]*" title="enter the alphbets only" placeholder="references by" onChange={this.handleChange} /></label><br />
          <button type="submit" class="btn btn-outline-primary mr-3" href="#">Register</button>
          <button type="reset" class="btn btn-outline-primary mr-1" href="#" onClick={this.handleClear}>Cancel</button>
        </form>
      </div>
    )
  }
}
