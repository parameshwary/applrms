import React from 'react';
import axios from 'axios';
class UpdateInterview extends React.Component{
    updateInter={
        candidateId:'',
        modeOfInterview:'',
        dateOfInterview:'',
        location:'',
        reccommendation:'',
        interviewPanel:'',
        referencesBy:'',
        id:Number
        

    };
    state = {
      item: this.updateInter
    }
    constructor(props) {
        super(props);
        
        
        
}
componentDidMount()
        {
            let cid=this.props.match.params.id;
            console.log("id",cid);
             axios.get(`http://localhost:9080/interview/`+cid).then(res => {
                console.log(res);
                this.setState({
                   item: res.data
                   });
        });
        }
       handleChange =event=>{
         let name=event.target.name;
         let value=event.target.value;
         let editinter={...this.state.item}
         editinter[name]=value;
         this.setState({
           item:editinter
         })
         
       }
        handleSubmit = event => {
          event.preventDefault();
         // console.log()
        let interview=this.state.item;
        
          if(window.confirm("Do you want to update???")){
          axios.put("http://localhost:9080/interview",interview)
          // (res => {
          //   console.log(res).catch(error=>console.log(error));
          //   console.log(res.data);
          // })
           alert("updated successfully");
           window.location.replace("http://localhost:3000/ViewInterview");
          }else{

          }        }
        render(){
            return (
                <form onSubmit={this.handleSubmit} class="col-lg-6 offset-lg-3">
                <label>Candidate Id:  <input type="text" class="form-control" name="candidateId" placeholder="candidateId" disabled value={this.state.item.candidateId} onChange={this.handleChange} /></label><br />
                <label>Mode Of Interview:  <input type="text" class="form-control" placeholder="Mode of Interview" name="modeOfInterview" value={this.state.item.modeOfInterview} onChange={this.handleChange} /></label><br />
                <label>Date OfInterview:  <input type="date" class="form-control" placeholder="Enter the Date" name="dateOfInterview" value={this.state.item.dateOfInterview} onChange={this.handleChange} /></label><br />
                <label>Location:  <input type="text" class="form-control" name="location" placeholder="location" value={this.state.item.location} onChange={this.handleChange} /></label><br />
                <label>Reccommendation:  <input type="text" class="form-control" name="reccommendation" placeholder="reccommendation" value={this.state.item.reccommendation} onChange={this.handleChange} /></label><br />
                <label>Interview Panel:  <input type="text" class="form-control" name="interviewPanel" placeholder="interview" value={this.state.item.interviewPanel} onChange={this.handleChange} /></label><br />
                <label>References By:  <input type="text" class="form-control" name="referencesBy" placeholder="references by" value={this.state.item.referencesBy} onChange={this.handleChange} /></label><br />
                <button type="submit" class="btn btn-outline-primary mr-3" href="#">Update</button>
                {/* <button type="submit" class="btn btn-outline-primary mr-1" href="#">Cancel</button> */}
              </form>
            )
        }
}
export default UpdateInterview;