import React from 'react';
import axios from 'axios';
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import {Link} from 'react-router-dom';
import Icon from '@material-ui/core/Icon';

class ViewInterview extends React.Component {
  state = {
    interview: [],
    i2: []
  };

  componentDidMount() {
    axios.get(`http://localhost:9080/interview/all`).then(res => {
      console.log(res);
      this.setState({ interview: res.data, i2:res.data })
    });
  }
  handleRemove = event => {
  const c=event.target.value;
    console.log(event.target.value);
    if(window.confirm("Do you want to delete???")){
    event.preventDefault();
    axios.delete("http://localhost:9080/interview/"+c)
      .then(res => {
        console.log(res);
      })
      .catch((err) => {
       // console.log(err);
      })
      alert("Deleted successfully");
      window.location.reload();
  }else{

  }        
}
handleSearch = event => {
  let filterinterview = []
  filterinterview = this.state.i2.filter((interview) => {

      return (interview.candidateId.toString().startsWith(event.target.value))
      //console.log(this.state.candidate);
  })
  this.setState({
      interview: filterinterview
  })

}

handleRefresh = event => {
  axios.get(`http://localhost:9080/interview/all`).then(res => {
      console.log(res);
      this.setState({ interview: res.data })
    });

}
  // handleEdit=event=>{
  //   const d=event.target.value;
  //   console.log(event.target.value);
  //   event.preventDefault();
  //   axios.put("http://localhost:9080/interview/"+d).then(res=>{
  //     console.log(res);
  //   })
  //   .catch((err)=>{
  //      //console.log(err);
  //   })
  // }
  renderFlippy() {
    const flippyMaker = this.state.interview.map(i => {
      let source="/interview/edit/"+i.id;
        
      return (
        <div className="alignflippyspace">

          <Flippy 
            flipOnHover={true}
            flipOnClick={false}
            flipDirection="vertical"

            style={{ width: '300px', height: '250px' }}
          >
            <FrontSide
             className="applyfrontcolor"
            >
              <div key={i.candidateId}><b> InterviewId:</b>{i.candidateId}</div>

            </FrontSide>
            <BackSide
              className="applybackcolor">
              <div key={i}>
                <tr>
                  <td><b> ModeOfInterview:</b></td>
                  <td>{i.modeOfInterview}</td>
                </tr>
                <tr><td><b>DateOfInterview:</b></td><td>{i.dateOfInterview}</td></tr>
                <tr><td><b> Location:</b></td><td>{i.location}</td></tr>
                <tr><td><b>Reccommendation:</b></td><td>{i.reccommendation}</td></tr>
                <tr><td><b>InterviewPanel:</b></td><td>{i.interviewPanel}</td></tr>
                <tr><td><b> ReferencesBy:</b></td><td>{i.referencesBy}</td></tr>
                <Link to={source}> <button class="btn btn-outline-primary mr-3" href="#">Update</button></Link>
                <button value={i.id} onClick={this.handleRemove} class="btn btn-outline-primary mr-3" href="#">Delete</button>


              </div>

            </BackSide>
          </Flippy>
        </div>


      );
    })

    return flippyMaker;
  }


  render() {
    return (
      <div class="alignflippy">
        <div class="search"> <input type="text" placeholder="enter aadhar id" name="search" onChange={this.handleSearch}>

          </input>
        </div>
        <div>
        <button onClick={this.handleRefresh}>Refresh</button>
      </div>
        {this.renderFlippy()}
      </div>
    )
  }

}


export default ViewInterview;