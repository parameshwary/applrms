import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  
} from "react-router-dom";
import Candidate from './Candidate/Candidate';
import Interview from './Interview/Interview';
import InterviewRegistration from './Interview/InterviewRegistration';
import Navbar from './Navbar';
import Home from './Home';
import ViewInterview from './Interview/ViewInterview';
// import DeleteInterview from './Interview/DeleteInterview';
import Register from './Candidate/Register';
import ViewCandidate from './Candidate/ViewCandidate';
import './App.css';
import UpdateInterview from './Interview/UpdateInterview';
import UpdateCandidate from './Candidate/UpdateCandidate';
const App =()=> {
  return (
    <Router>
      <div>
       <Navbar/>
        <Switch>
          {/* <Route exact path="/" component={Navbar}/> */}
          <Route path="/Home">
            <Home/>
            </Route>
         
          <Route exact path="/Candidate">
            <Candidate />
          </Route>
          <Route exact path="/Interview">
            <Interview />
          </Route>
          <Route  exact path="/InterviewRegistration">
            <InterviewRegistration/>
            </Route>
            <Route exact path="/ViewInterview">
              <ViewInterview/>
              </Route>
         {/* <Route exact path="/DeleteInterview">
           <DeleteInterview/>
           </Route> */}
           <Route exact path="/Register">
             <Register/>
             </Route>
             <Route exact path="/ViewCandidate">
               <ViewCandidate/>
               </Route>
               <Route exact path="/interview/edit/:id" component={UpdateInterview}/>
               <Route exact path="/candidate/edit/:id" component={UpdateCandidate}/>
        </Switch>
      </div>
    </Router>
  );
  }
  export default App;


