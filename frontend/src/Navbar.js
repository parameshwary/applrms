import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link to="/Home" className="navbar-brand">
        Home
        </Link>
      <Link to="/Candidate" className="navbar-brand">
        Candidate
      </Link>
      <Link to="/Interview" className="navbar-brand">
        Interview
      </Link>
     
        </nav>
  );
};
export default NavBar;