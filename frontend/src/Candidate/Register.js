import React from 'react';
import axios from 'axios';

export default class Register extends React.Component {

  state = {
    candidateId: '',
    firstName: '',
    lastName: '',
    mobile: '',
    alternativeMobile: '',
    email: '',
    totalExperience: '',
    relavantExperience: '',
    primarySkills: null,
    secondarySkills: null,
    qualification: '',
    additionalQualification: '',
    expectedCTC: '',
    currentCTC: '',
    positionApplied: '',
    address: null,
    gender: '',
    dateOfBirth: '',

  };

  handleChange = event => {
    if (event.target.name === "primarySkills" || event.target.name === "secondarySkills" || event.target.name === "address" || event.target.name === "") {
      this.setState({ [event.target.name]: [event.target.value] });
    }
    // else if(event.target.name=="secondary")
    // {
    //   this.setState({ [event.target.name]: [event.target.value] });
    // }
    // else if(event.target.name=="primarySkills")
    // {
    //   this.setState({ [event.target.name]: [event.target.value] });
    // }
    else
      this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = event => {
    event.preventDefault();
    let candidate = this.state;
    console.log(candidate);

    axios.post('http://localhost:9080/candidate', candidate)
    .then(res=>{
      console.log(res.data);
      if(res.data==="candidate Already Exit"){
        alert("already exist");
      }
      else{
        alert("added")
        window.location.replace("http://localhost:3000/ViewCandidate");
      }
    })
  }

    // axios.post('http://localhost:9080/candidate', candidate)
    // (res => {
    //   console.log(res).catch(error=>console.log(error));
    //   console.log(res.data);
    // })
    // .then(res=>{
    //   if(res.status==)
    // })
  //   alert("Form submitted successfully");
  //   window.location.replace("http://localhost:3000/ViewCandidate");
  // }
  // handleSubmit = event =>{
  //   event.preventDefault();
  //   axios.post('http://localhost:9080/candidate', this.state)
  //   .then(res=>{
  //     if(res.status==200){
  //       alert("Form submitted successfully");
  //       window.location.replace("http://localhost:3000/ViewCandidate");
  //     }
  //     else{
  //       alert("invalidate candidate Id");
  //     }
  //   }).catch(res=>{
  //     alert("form not submitted");
  //   })
  
  
  // }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit} class="col-lg-6 offset-lg-3" >

          <label><span class="required">*</span> Candidate Id:  <input type="text" class="form-control" name="candidateId" placeholder="candidateId" pattern="[0-9]{12}" title="enter the numbers only" onChange={this.handleChange} maxLength="12" required /></label><br />
          <label><span class="required">*</span> First Name:  <input type="text" class="form-control" name="firstName" pattern="[A-Za-z//s]*" title="enter the alphbets only" placeholder="FirstName" onChange={this.handleChange}   required /></label><br />
          <label><span class="required">*</span> Last Name:  <input type="text" class="form-control" name="lastName" pattern="[A-Za-z//s]*" title="enter the alphabets only" placeholder="LastName" onChange={this.handleChange} maxlength="10" required /></label><br />
          <label><span class="required">*</span> Mobile: <input type="text" class="form-control" name="mobile" pattern="[0-9]{10}" title="enter 10 digits numbers only" placeholder="mobile" onChange={this.handleChange} maxlength="10" required /></label><br />
          <label>Alternative Mobile:  <input type="text" class="form-control" placeholder="AlternativeMobile" pattern="[0-9]{10}" title="enter the numbers only" name="alternativeMobile" onChange={this.handleChange} maxlength="10" /></label><br />
          <label><span class="required">*</span> Email: <input type="email" name="email" class="form-control" placeholder="email"  onChange={this.handleChange} required /></label><br />
          <label><span class="required">*</span> Total Experience:<i><h6>(in years) </h6></i> <input type="text" class="form-control" placeholder="TotalExperience" pattern="[0-9]{2}" title="enter the 2 digits numbers only"  name="totalExperience" onChange={this.handleChange} maxlength="2" required /></label><br />
          <label><span class="required">*</span> Relavant Experience:<i><h6>(in years) </h6></i>  <input type="text" class="form-control" placeholder="RelavantExperience" pattern="[0-9]{2}" title="enter the 2 digits numbers only" name="relavantExperience" maxlength="2" onChange={this.handleChange} /></label><br />
          <label><span class="required">*</span> Primary Skills:  <input type="text" class="form-control" name="primarySkills" placeholder="Primary Skills" onChange={this.handleChange} required /></label><br />
          <label>Secondary Skills: <input type="text" class="form-control" name="secondarySkills" placeholder="Secondary Skills" onChange={this.handleChange} /></label><br />
          <label ><span class="required">*</span> Qualification: <input type="text" class="form-control" name="qualification" placeholder="Qualification" onChange={this.handleChange} required /></label><br />
          <label>Additional Qualification: <input type="text" class="form-control" name="additionalQualification" placeholder="Additional Qualification" onChange={this.handleChange} /></label><br />
          <label><span class="required">*</span> Expected CTC: <input type="number"  class="form-control" name="expectedCTC" pattern="[0-9]{3}" title="enter the digits only" placeholder="Expected CTC" onChange={this.handleChange} required /></label><br />
          <label> <span class="required">*</span>Current CTC: <input type="number" class="form-control" name="currentCTC" placeholder="Current CTC" onChange={this.handleChange} required /></label><br />
          <label><span class="required">*</span>Position Applied: <input type="text" class="form-control" name="positionApplied" placeholder="Position Applied" onChange={this.handleChange} required /></label><br />
          <label><span class="required">*</span> Address: <input type="text" class="form-control" class="form-control" name="address" placeholder="Address" onChange={this.handleChange} required /></label><br />
          <label><span class="required">*</span>Gender</label>
          <label><input type="radio" class="radio" name="gender" value="male" onClick={this.handleChange} />Male</label>
          <label><input type="radio" class="radio" name="gender" value="female" onClick={this.handleChange} />Female</label>
          <label><input type="radio" class="radio" name="gender" value="other" onClick={this.handleChange} />Others</label><br />

          <label><span class="required">*</span>Date Of Birth: <input type="date" name="dateOfBirth" class="form-control" placeholder="Date Of Birth" onChange={this.handleChange} required /></label><br />

          <button type="submit" class="btn btn-outline-primary mr-3">Register</button>
          <button type="reset" class="btn btn-outline-primary mr-1" href="#">Cancel</button>

        </form>
      </div>
    )
  }
}
