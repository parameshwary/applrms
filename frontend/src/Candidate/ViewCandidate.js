import React from 'react';
import axios from 'axios';
import Flippy, { FrontSide, BackSide } from 'react-flippy';
import { Link } from 'react-router-dom';


class ViewCandidate extends React.Component {
  state = {
    candidate: [],
    c2: []


  };

  componentDidMount() {
    console.log(this.state);
    axios.get(`http://localhost:9080/candidate/all`).then(res => {
      console.log(res.status)

      this.setState({ candidate: res.data, c2: res.data })
    });
  }
  handleRefresh = event => {
    axios.get(`http://localhost:9080/candidate/all`).then(res => {

      this.setState({ candidate: res.data })
    });

  }
  handleRemove = event => {
    const c = event.target.value;
    console.log(event.target.value);
    if (window.confirm("Do you want to delete???")) {
      event.preventDefault();
      axios.delete("http://localhost:9080/candidate/" + c)
        .then(res => {
          console.log(res);
        })
        .catch((err) => {
          // console.log(err);
        })
      alert("Deleted successfully");
      window.location.reload();
    } else {

    }
  }

  handleSearch = event => {
    let filtercandidate = []
    filtercandidate = this.state.c2.filter((candidate) => {

      return (candidate.candidateId.toString().startsWith(event.target.value))
      console.log(this.state.candidate);
    })
    this.setState({
      candidate: filtercandidate
    })

  }
  sorting = () => {
    const sortbyname = this.state.candidate.sort(function (a, b) {
      const firstName1 = a.firstName.toLowerCase();
      const firstName2 = b.firstName.toLowerCase();
      if (firstName1 > firstName2) {
        return 1;
      }
      else if (firstName2 < firstName2) {
        return -1;
      }
      return 0;
    })
}
renderFlippy() {
  const flippyMaker = this.state.candidate.map(c => {
    let source = "/candidate/edit/" + c.id;
    return (


      <div className="alignflippyspace">


        <Flippy
          flipOnHover={false}
          flipOnClick={true}
          flipDirection="vertical"

          style={{ width: '350px', height: '500px' }}
        >
          <FrontSide className="applyfrontcolor"

          >
            <div key={c.candidateId}> CandidateId:{c.candidateId}</div>

          </FrontSide>
          <BackSide
            className="applybackcolor">
            <div key={c}>
              <tr>
                <td><b>First Name:</b></td>
                <td>{c.firstName}</td>
              </tr>
              <tr><td><b>Last Name:</b></td><td>{c.lastName}</td></tr>
              <tr><td><b>Mobile:</b></td><td>{c.mobile}</td></tr>
              <tr><td><b>Alternative Mobile:</b></td><td>{c.alternativeMobile}</td></tr>
              <tr><td><b>Email:</b></td><td>{c.email}</td></tr>
              <tr><td><b>Total Experience:</b></td><td>{c.totalExperience}</td></tr>
              <tr><td><b>Relavant Experience:</b></td><td>{c.relavantExperience}</td></tr>
              <tr><td><b>Primary Skills:</b></td><td>{c.primarySkills}</td></tr>
              <tr><td><b>Secondary Skills:</b></td><td>{c.secondarySkills}</td></tr>
              <tr><td><b>Qualification:</b></td><td>{c.qualification}</td></tr>
              <tr><td><b>AdditionalQualification:</b></td><td>{c.additionalQualification}</td></tr>
              <tr><td><b>Expected CTC:</b></td><td>{c.expectedCTC}</td></tr>
              <tr><td><b>Current CTC:</b></td><td>{c.currentCTC}</td></tr>
              <tr><td><b>Position Applied:</b></td><td>{c.positionApplied}</td></tr>
              <tr><td><b>Address:</b></td><td>{c.address}</td></tr>
              <tr><td><b>Gender:</b></td><td>{c.gender}</td></tr>
              <tr><td><b>Date Of Birth:</b></td><td>{c.dateOfBirth}</td></tr><br />

              <Link to={source}> <button class="btn btn-outline-primary mr-3" href="#">Update</button></Link>
              <button value={c.candidateId} onClick={this.handleRemove} class="btn btn-outline-primary mr-3" href="#">Delete</button>

            </div>

          </BackSide>
        </Flippy>
      </div>


    );
  })

  return flippyMaker;
}


render() {
  return (
    <div class="alignflippy">
      <div class="search">
        <input type="text" placeholder="enter aadhar id" name="search" onChange={this.handleSearch} />

      </div>
      <div>
        <button type="submit" onClick={this.sorting}>Sort</button>
      </div>
      <div>
        <button onClick={this.handleRefresh}>Refresh</button>
      </div>
      {this.renderFlippy()}
    </div>
  )
}

}


export default ViewCandidate;