import React from 'react';
import axios from 'axios';
class UpdateCandidate extends React.Component {
    updateCandid = {
        candidateId: '',
        firstName: '',
        lastName: '',
        mobile: '',
        alternativeMobile: '',
        email: '',
        totalExperience: '',
        relavantExperience: '',
        primarySkills: null,
        secondarySkills: null,
        qualification: '',
        additionalQualification: '',
        expectedCTC: '',
        currentCTC: '',
        positionApplied: '',
        address: null,
        gender: '',
        dateOfBirth: '',

    };
    state = {
        item: this.updateCandid
    }
    componentDidMount() {
        let cid = this.props.match.params.id;
        console.log("id", cid);
        axios.get(`http://localhost:9080/candidate/` + cid).then(res => {
            console.log(res);
            this.setState({
                item: res.data
            });
        });
    }
    handleChange = event => {
        let name = event.target.name;
        let value = event.target.value;
        let editcandid = { ...this.state.item }
        editcandid[name] = value;
        this.setState({
            item: editcandid
        })

    }
    handleSubmit = event => {
        event.preventDefault();
        // console.log()
        let candidate = this.state.item;
        if (window.confirm("Do you want to update???")) {
            axios.put("http://localhost:9080/candidate", candidate)
            // (res => {
            //   console.log(res).catch(error=>console.log(error));
            //   console.log(res.data);
            // })
            alert("updated successfully");
            window.location.replace("http://localhost:3000/ViewCandidate");
        } else {

        
    }
}
render() {

    return (
        <form onSubmit={this.handleSubmit} class="col-lg-6 offset-lg-3">
            <label>Candidate Id:  <input type="text" class="form-control" name="candidateId" placeholder="candidateId" disabled value={this.state.item.candidateId} onChange={this.handleChange} /></label><br />
            <label>First Name:  <input type="text" class="form-control" name="firstName" placeholder="FirstName" value={this.state.item.firstName} onChange={this.handleChange} /></label><br />
            <label>Last Name:  <input type="text" class="form-control" name="lastName" placeholder="LastName" value={this.state.item.lastName} onChange={this.handleChange} /></label><br />
            <label>Mobile: <input type="text" class="form-control" name="mobile" placeholder="mobile" value={this.state.item.mobile} onChange={this.handleChange} /></label><br />
            <label>Alternative Mobile:  <input type="text" class="form-control" placeholder="AlternativeMobile" value={this.state.item.alternativeMobile} name="alternativeMobile" onChange={this.handleChange} /></label><br />
            <label>Email: <input type="text" name="email" class="form-control" placeholder="email" value={this.state.item.email} onChange={this.handleChange} /></label><br />
            <label>Total Experience:  <input type="text" class="form-control" placeholder="TotalExperience" value={this.state.item.totalExperience} name="totalExperience" onChange={this.handleChange} /></label><br />
            <label>Relavant Experience:  <input type="text" class="form-control" placeholder="RelavantExperience" value={this.state.item.relavantExperience} name="relavantExperience" onChange={this.handleChange} /></label><br />
            <label>Primary Skills:  <input type="text" class="form-control" name="primarySkills" placeholder="Primary Skills" value={this.state.item.primarySkills} onChange={this.handleChange} /></label><br />
            <label>Secondary Skills: <input type="text" class="form-control" name="secondarySkills" placeholder="Secondary Skills" value={this.state.item.secondarySkills} onChange={this.handleChange} /></label><br />
            <label>Qualification: <input type="text" class="form-control" name="qualification" placeholder="Qualification" value={this.state.item.qualification} onChange={this.handleChange} /></label><br />
            <label>Additional Qualification: <input type="text" class="form-control" name="additionalQualification" value={this.state.item.additionalQualification} placeholder="Additional Qualification" onChange={this.handleChange} /></label><br />
            <label>Expected CTC: <input type="text" class="form-control" name="expectedCTC" placeholder="Expected CTC" value={this.state.item.expectedCTC} onChange={this.handleChange} /></label><br />
            <label>Current CTC: <input type="text" class="form-control" name="currentCTC" placeholder="Current CTC" value={this.state.item.currentCTC} onChange={this.handleChange} /></label><br />
            <label>Position Applied: <input type="text" class="form-control" name="positionApplied" placeholder="Position Applied" value={this.state.item.positionApplied} onChange={this.handleChange} /></label><br />
            <label>Address: <input type="text" class="form-control" name="address" placeholder="Address" value={this.state.item.address} onChange={this.handleChange} /></label><br />
            <label>Gender</label>
            
            <label><input type="radio" class="radio" name="gender" value={this.state.item.gender} onClick={this.handleChange} />Male</label>
            <label><input type="radio" class="radio" name="gender" value={this.state.item.gender} onClick={this.handleChange} />Female</label>
            <label><input type="radio" class="radio" name="gender" value={this.state.item.gender} onClick={this.handleChange} />Other</label><br/>
            <br />
            <label>Date Of Birth: <input type="text" name="dateOfBirth" class="form-control" placeholder="Date Of Birth" value={this.state.item.dateOfBirth} onChange={this.handleChange} /></label><br />
            <button type="submit" class="btn btn-outline-primary mr-3" href="#">Update</button>

        </form>
    )
}


}
export default UpdateCandidate;