package com.digitallschool.training.spiders.model;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class InterviewRepository {
	@Autowired
	DataSource ds;
	public Interview findInterview(int cid) {
		Interview interview=null;
		try (Connection con = ds.getConnection();
				PreparedStatement st = con.prepareStatement("select * from interview where candidate_id=?");
				) {
			st.setInt(1, cid);
			
			ResultSet rs = st.executeQuery();
			if(rs.next()) {
				interview=new Interview(rs.getInt(1),rs.getString(2),rs.getDate(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7));
				return interview;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return interview;
	}
	public List<Interview> allInterviews() throws SQLException {
		List<Interview> interviews = null;
		try (Connection con = ds.getConnection();
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery("select * from interview")) {
			interviews = new ArrayList<>();
			while (rs.next()) {
				interviews.add(new Interview(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return interviews;
	}

	public boolean addInterview(Interview i) {
		try (Connection con = ds.getConnection();
				PreparedStatement pst = con.prepareStatement("INSERT INTO interview VALUES(?, ?, ?, ?, ?,?,?)");) {

			pst.setInt(1, i.getcandidateId());
			pst.setString(2, i.getmodeOfInterview());
			pst.setDate(3, (Date)i.getdateOfInterview());
			pst.setString(4, i.getLocation());
			pst.setString(5, i.getReccommendation());
			pst.setString(6, i.getinterviewPanel());
			pst.setString(7, i.getreferencesBy());

			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteInterview(int candidateId) {
		try (Connection con = ds.getConnection();
				PreparedStatement pst = con.prepareStatement("DELETE FROM interview WHERE candidate_id=?");) {

			pst.setInt(1, candidateId);

			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateInterview(int candidateId,Interview i) {
		try (Connection con = ds.getConnection();
				PreparedStatement pst = con.prepareStatement(
						"UPDATE interview SET mode_of_interview=?,date_of_interview=?,location=?,reccommendation=?,interview_panel=?,references_by=? WHERE candidate_id=?");) {

			pst.setString(1, i.getmodeOfInterview());
			pst.setDate(2, Date.valueOf(i.getdateOfInterview().toString()));
			pst.setString(3, i.getLocation());
			pst.setString(4, i.getReccommendation());
			pst.setString(5, i.getinterviewPanel());
			pst.setString(6, i.getreferencesBy());
			pst.setInt(7, i.getcandidateId());

			pst.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}
	
	    }








