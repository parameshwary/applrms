package com.digitallschool.training.spiders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RMSApplication {
	public static void main(String[] args) {
		SpringApplication app=new SpringApplication(RMSApplication.class);
		app.run(args);
	}

}
