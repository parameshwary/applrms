package com.digitallschool.training.spiders.model;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/interview")
public class InterviewController {
	@Autowired
	InterviewService interviewservice;

	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Interview> allInterviews() throws SQLException {
		return interviewservice.allInterviews();
	}
	@GetMapping("{cid}")
	public Interview findInterview(@PathVariable int cid) {
		Interview interview=null;
		interview=interviewservice.findnterview(cid);
		return interview;
	}

	@DeleteMapping({"/{candidateId}"})
	public ResponseEntity<String> deleteCandidate(@PathVariable int candidateId) {
		interviewservice.deleteInterview(candidateId);
		return ResponseEntity.ok("Successfully Deleted");
	}
	
	@PostMapping
	public ResponseEntity addInterview(@RequestBody Interview interview, BindingResult result) {

		System.out.println(interview.getdateOfInterview());
		if (!result.hasErrors() && interviewservice.addInterview(interview)) {
			return ResponseEntity.ok("Successfully Added");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body("Incomplete or Invalid Data: " + result.toString());
		}
		
	}
	@PutMapping
	public ResponseEntity updateInterview(@RequestBody Interview interview , BindingResult result) {
		int id=interview.getcandidateId();
		if (!result.hasErrors() && interviewservice.updateInterview(id,interview )) {
			return ResponseEntity.ok("Successfully Updated");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body("Incomplete or Invalid Data: " + result.toString());
		}
	}
	
	
    
	@ModelAttribute("interview")
	public Interview setInterview()
	{
		return new Interview();
	}
	

}
