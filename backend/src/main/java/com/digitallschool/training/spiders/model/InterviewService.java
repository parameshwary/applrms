package com.digitallschool.training.spiders.model;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class InterviewService {
	@Autowired
	InterviewRepository interviewrepository;

	public List<Interview> allInterviews() throws SQLException {
		return interviewrepository.allInterviews();
	}

	public boolean addInterview(Interview i) {
		return interviewrepository.addInterview(i);
	}

	public boolean deleteInterview(int candidateId) {
		return interviewrepository.deleteInterview(candidateId);
	}

	public boolean updateInterview(int candidateId,Interview i) {
		return interviewrepository.updateInterview(candidateId, i);
	}
	public Interview findnterview(int cid) {
		return interviewrepository.findInterview(cid);
	}
}
