package com.digitallschool.training.spiders.candidatemodel;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.stereotype.Repository;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import static com.mongodb.client.model.Filters.eq;

@Repository
public class CandidateRepository {
	@Autowired
	MongoDbFactory md;
	public Candidate findCandidate(int candidateId){
	       Candidate temp=null;
	       // Consumer<Candidate> candidate=e->e;
	       CodecRegistry codec=CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
	                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
	      MongoCollection collection=md.getDb("parameshwary").getCollection("candidate", Candidate.class)
			.withCodecRegistry(codec);
	        FindIterable<Candidate> iter=collection.find(eq("candidateId",candidateId),Candidate.class);
	        for(Candidate c:iter){
	            temp=c;
	        }
	       return temp;
	   }

	public List<Candidate> allCandidates() {
		List<Candidate> candidates = new ArrayList<Candidate>();
		CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
		Consumer<Candidate> candidate = e -> candidates.add(e);
		md.getDb("parameshwary").getCollection("candidate", Candidate.class).withCodecRegistry(codec).find()
				.forEach(candidate);
		return candidates;

	}

	public boolean addCandidate(Candidate candidate) {
		try {
			CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
					CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
			md.getDb("parameshwary").getCollection("candidate", Candidate.class).withCodecRegistry(codec)
					.insertOne(candidate);
			return true;
		} catch (MongoException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteCandidate(int id) {
		try {
			CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
					CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
			md.getDb("parameshwary").getCollection("candidate", Candidate.class).withCodecRegistry(codec)
					.deleteOne(eq("candidateId", id));
			return true;
		} catch (MongoException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateCandidate(int candidateId, Candidate candidate) {
		try {
			CodecRegistry codec = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
					CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
			md.getDb("parameshwary").getCollection("candidate", Candidate.class).withCodecRegistry(codec)
					.replaceOne(eq("candidateId", candidateId), candidate);
			return true;

		} catch (MongoException e) {
			e.printStackTrace();
		}
		return false;
	}
}
