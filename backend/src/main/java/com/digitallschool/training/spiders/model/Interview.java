package com.digitallschool.training.spiders.model;

import java.sql.Date;

public class Interview {
	private int candidateId;
	private String modeOfInterview;
	private Date dateOfInterview;
	private String location;
	private String reccommendation;
	private String interviewPanel;
	private String referencesBy;
	public Interview(int candidateId,String modeOfInterview,Date dateOfInterview,String location,String reccommendation,String interviewPanel,String referencesBy)
	{
		this.candidateId=candidateId;
		this.modeOfInterview=modeOfInterview;
		this.dateOfInterview=dateOfInterview;
		this.location=location;
		this.reccommendation=reccommendation;
		this.interviewPanel=interviewPanel;
		this.referencesBy=referencesBy;
	}
	public Interview()
	{
		super();
	}
	public int getcandidateId() {
		return candidateId;
	}
	public void setcandidateId(int candidateId) {
		this.candidateId = candidateId;
	}
	public String getmodeOfInterview() {
		return modeOfInterview;
	}
	public void setmodeOfInterview(String modeOfInterview) {
		this.modeOfInterview = modeOfInterview;
	}
	public Date getdateOfInterview() {
		return dateOfInterview;
	}
	public void setdateOfInterview(Date dateOfInterview) {
		this.dateOfInterview = dateOfInterview;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getReccommendation() {
		return reccommendation;
	}
	public void setReccommendation(String reccommendation) {
		this.reccommendation = reccommendation;
	}
	public String getinterviewPanel() {
		return interviewPanel;
	}
	public void setinterviewPanel(String interviewPanel) {
		this.interviewPanel = interviewPanel;
	}
	public String getreferencesBy() {
		return referencesBy;
	}
	public void setreferencesBy(String referencesBy) {
		this.referencesBy = referencesBy;
	}

}
